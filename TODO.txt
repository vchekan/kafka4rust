* IPv6 support
* Instrumentation
* Tracing

(+) Tokio
(+) Serde
* Client API
* CLI tools
* Wrappers
    ** C#
    ** Java
* Integration testing

* Tcp: nodelay, experiment with tx,rx buffer size