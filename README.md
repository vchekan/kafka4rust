== Goals
* High quality, high performance and reliability kafka client
* Implement modern async paradigm for best performance and low resource usage
* Real life oriented implementation: instrumentation and troubleshooting friendly, good failure handling by design
  and not afterthought
* Easy way to integrate with other languages